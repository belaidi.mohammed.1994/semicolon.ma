import layoutReducer from "./layout";


const reducers = {
  layout: layoutReducer,
};
export default reducers;
