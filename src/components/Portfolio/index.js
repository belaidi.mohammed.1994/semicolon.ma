import React, { useState } from "react";
import { motion } from "framer-motion";
// @mui
import {
  Stack,
  Card,
  CardMedia,
  CardContent,
  styled,
  Typography,
  IconButton,
} from "@mui/material";
import { Icon } from "@iconify/react";
// components
import HelloCard from "./hello_card";

// ----------------------------------------------------------------------
const RootStyle = styled("div")(({ theme }) => ({
  background: "#EDF2F8",
  padding: theme.spacing(10, 0),
  [theme.breakpoints.up("md")]: {
    // height: "100vh",
    padding: 0,
  },
}));
// ----------------------------------------------------------------------

export default function PortfolioSection() {
  return (
    <RootStyle>
      <Stack
        sx={{
          p: 10,
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
        spacing={6}
      >
        <Typography sx={{ fontSize: "2.75rem", fontWeight: 700 }}>
          My Creative{" "}
          <span
            style={{ fontSize: "2.75rem", fontWeight: 700, color: "#313bac" }}
          >
            Portfolio{" "}
          </span>
          Section
        </Typography>
        <Stack
          direction="row"
          sx={{ width: "90%", justifyContent: "center" }}
          spacing={2}
        >
          <FilterItem label="All" selected={false} />
          <FilterItem label="Web App" selected={true} />
          <FilterItem label="Mobile App" selected={false} />
        </Stack>
        <Stack
          direction="row"
          sx={{
            width: "90%",
            justifyContent: "center",
            flexWrap: "wrap",
            gap: 2,
          }}
          spacing={2}
        >
          <PortfolioItem />
          <PortfolioItem />
          <PortfolioItem />
          <PortfolioItem />
          <PortfolioItem />
        </Stack>
      </Stack>
    </RootStyle>
  );
}

const FilterItem = ({ label, selected, onClick }) => {
  return (
    <IconButton
      disableRipple
      size={"small"}
      onClick={onClick}
      sx={{
        background: selected ? "#313bac" : "#fff",
        borderRadius: 0.8,
      }}
    >
      <Typography
        variant={"caption"}
        sx={{
          marginX: 1,
          color: selected ? "#FFF" : "#000",
          fontSize: "0.8rem",
          fontWeight: 700,
        }}
      >
        {label}
      </Typography>
    </IconButton>
  );
};
const PortfolioItem = () => {
  return (
    <Card sx={{ borderRadius: 1, p: 2, width: 300 }}>
      <CardMedia
        sx={{ height: 240, borderRadius: 1 }}
        image="/static/portfolio/portfolio01.png"
        title="Portfolio 01"
      />
      <CardContent
        sx={{ display: "flex", justifyContent: "center", flexWrap: "wrap" }}
      >
        <Stack spacing={1}>
          <Typography gutterBottom sx={{ fontSize: "1.2rem", fontWeight: 600 }}>
            Mobile App Demo
          </Typography>
          <Stack
            direction="row"
            sx={{
              justifyContent: "flex-start",
              flexWrap: "wrap",
              gap: 1,
            }}
          >
            <TagItem label="Web App" />
            <TagItem label="Mobile App" />
            <TagItem label="Lorem ipsum dolor." />
            <TagItem label="Logistic companies" />
            <TagItem label="App IOS" />
          </Stack>
        </Stack>
      </CardContent>
    </Card>
  );
};
const TagItem = ({ label, onClick }) => {
  return (
    <IconButton
      disableRipple
      size={"small"}
      onClick={onClick}
      sx={{
        background: "#edf2f8",
        borderRadius: 0.8,
      }}
    >
      <Typography
        variant={"caption"}
        sx={{
          marginX: 1,
          color: "#000",
          fontSize: "0.8rem",
          fontWeight: 600,
        }}
      >
        {label}
      </Typography>
    </IconButton>
  );
};
