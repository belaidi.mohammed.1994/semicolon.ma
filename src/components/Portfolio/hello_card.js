import React from "react";
// Mui
import { Card, Stack, Typography } from "@mui/material";

const HelloCard = () => {
  return (
    <Stack
      direction="row"
      sx={{
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 2,
        p: 2.5,
        background: "transparent",
        boxShadow: "0 0 20px rgba(0,0,0,.1)",
      }}
    >
      <Typography sx={{ fontSize: 35 }}>👋</Typography>
      <Stack>
        <Typography sx={{ color: "#6b7688", fontSize: 14 }}>
          Hello, I m
        </Typography>
        <Typography
          sx={{
            textAlign: "center",
            color: "#000",
            fontWeight: 700,
            fontSize: 30,
          }}
        >
          Belaidi Mohammed
        </Typography>
      </Stack>
    </Stack>
  );
};

export default HelloCard;
