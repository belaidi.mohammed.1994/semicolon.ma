import React, { useState } from "react";
// @mui
import {
  Stack,
  Container,
  styled,
  Typography,
  Button,
  Fab,
} from "@mui/material";
import { Icon } from "@iconify/react";
// components

export default function Skills() {
  return (
    <Stack
      sx={{
        p: 10,
        alignItems: "center",
        justifyContent: "center",
        flexWrap: "wrap",
      }}
      spacing={10}
    >
      <Typography sx={{ fontSize: "2.75rem", fontWeight: 700 }}>
        Skills
      </Typography>
      <Stack
        direction="row"
        sx={{
          justifyContent: "center",
          flexWrap: "wrap",
          gap: 6
        }}
      >
        <SkillItem icon={"/static/skills/html.png"} label="html" />
        <SkillItem icon={"/static/skills/css.png"} label="css" />
        <SkillItem icon={"/static/skills/js.png"} label="javascript" />
        <SkillItem icon={"/static/skills/nodejs.png"} label="nodejs" />
        <SkillItem icon={"/static/skills/react.png"} label="reactjs" />
        <SkillItem icon={"/static/skills/next.png"} label="nextjs" />
        <SkillItem icon={"/static/skills/mui.png"} label="materiel ui" />
        <SkillItem icon={"/static/skills/redux.png"} label="redux" />
        <SkillItem icon={"/static/skills/express.png"} label="expressjs" />
        <SkillItem icon={"/static/skills/graphql.png"} label="graphql" />
        <SkillItem icon={"/static/skills/apollo.png"} label="apollo graphql" />
        <SkillItem icon={"/static/skills/mongo.png"} label="mongodb" />
        <SkillItem icon={"/static/skills/git.png"} label="git" />
      </Stack>
    </Stack>
  );
}

const SkillItem = ({ icon, label }) => {
  return (
    <Stack spacing={1} sx={{ alignItems: "center" }}>
      <div
        style={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Fab sx={{ width: 90, height: 90, background: "#EFF0FF" }}>
          <img style={{ width: 50 }} src={icon} alt={label} />
        </Fab>
      </div>
      <Typography sx={{ fontSize: "0.8rem", color: "#6b7688" }}>
        {label}
      </Typography>
    </Stack>
  );
};
