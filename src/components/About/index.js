import React, { useState } from "react";
// @mui
import { Stack, Container, styled, Typography, Button } from "@mui/material";
import { Icon } from "@iconify/react";
// components

export default function About() {
  // Download CV
  const handleDownload = () => {
    const link = document.createElement("a");
    link.href = "/static/Belaidi mohammed.pdf"; // Replace with the path to your PDF file
    link.download = "Belaidi mohammed.pdf";
    link.click();
  };

  return (
    <Stack
      sx={{
        p: 10,
        display: "flex",
        alignItems: "center",
        justifyContent: "center",
      }}
      spacing={10}
    >
      <Typography sx={{ fontSize: "2.75rem", fontWeight: 700 }}>
        About{" "}
        <span
          style={{ fontSize: "2.75rem", fontWeight: 700, color: "#313bac" }}
        >
          Me
        </span>
      </Typography>
      <Stack
        direction="row"
        sx={{ width: "90%", justifyContent: "space-between" }}
        spacing={4}
      >
        <Stack spacing={3}>
          <Typography sx={{ fontSize: "1.5rem", fontWeight: 600 }}>
            Belaidi mohammed
          </Typography>
          <Typography sx={{ fontSize: "1rem", color: "#6b7688" }}>
            As a self-taught programmer with a background in electrical
            engineering, I have a strong foundation in both technical and
            analytical skills. I am proficient in a variety of technologies
            including Typescript, Node.js, Redis, MongoDB, Apollo Server,
            React.js, Next.js, Material UI, Redux, and React Router, I possess a
            broad range of technical skills and knowledge that allow me to
            develop and maintain both the front-end and back-end of a web
            application. I am a proactive learner with a passion for solving
            problems and am always looking for new challenges and opportunities
            to grow my skills.
          </Typography>
          <Button
            onClick={handleDownload}
            variant={"outlined"}
            sx={{ width: 150 }}
          >
            Resume
          </Button>
        </Stack>

        <img
          style={{ width: 250, height: 250, borderRadius: "50%" }}
          src={"/static/my_image.png"}
          alt={"My image"}
        />
      </Stack>
    </Stack>
  );
}
