import React, { useEffect, useState } from "react";
// Next
import Image from "next/image";
// Mui
import { Box, Link, Stack, Toolbar, Typography } from "@mui/material";

const DesktopNavbar = ({ Link }) => {
  // Making top bar transparent
  const [transparent, setTransparent] = useState(true);

  useEffect(() => {
    const handleScroll = () => setTransparent(window.scrollY === 0);
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <Toolbar
      sx={{
        width: "95%",
        justifyContent: "space-between",
        alignItems: "center",
      }}
    >
      <Image height={35} width={170} src={"/static/logo.png"} alt={"Logo"} />
      <Stack direction="row" spacing={3} sx={{ alignItems: "center" }}>
        {Link?.map(({ name, to }, key) => (
          <LinkTypography
            key={key}
            transparent={transparent}
            name={name}
            to={to}
          />
        ))}
      </Stack>
      <Box sx={{ width: 200 }} />
    </Toolbar>
  );
};

const LinkTypography = ({ name, to }) => {
  return (
    <Link href={to} sx={{ p: 1, textDecoration: "none" }}>
      <Typography
        sx={{
          textTransform: "uppercase",
          fontWeight: 500,
          fontSize: 13,
          color: (theme) => theme.palette.grey[600],
        }}
      >
        {name}
      </Typography>
    </Link>
  );
};

export default DesktopNavbar;
