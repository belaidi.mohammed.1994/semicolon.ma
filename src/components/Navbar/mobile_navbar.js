import React from "react";
// Next
import Image from "next/image";
// Mui
import {
  Button,
  Drawer,
  IconButton,
  Stack,
  Toolbar,
  Typography,
} from "@mui/material";
import { Icon } from "@iconify/react";
// Redux
import { useDispatch, useSelector } from "react-redux";
import { toggleSidebar } from "../../redux/reducers/layout/sidebar/action";

const MobileNavbar = ({ Link }) => {
  // Redux
  const dispatch = useDispatch();

  return (
    <>
      <Toolbar
        sx={{
          justifyContent: "space-between",
          width: "100%",
        }}
      >
        <Image height={30} width={140} src={"/static/logo.png"} alt={"Logo"} />
        <IconButton
          sx={{ borderRadius: 4 }}
          onClick={() => dispatch(toggleSidebar())}
        >
          <Icon width={25} height={25} icon="material-symbols:menu-rounded" />
        </IconButton>
      </Toolbar>
      <Sidebar Link={Link} />
    </>
  );
};

const Sidebar = ({ Link }) => {
  // Redux
  const dispatch = useDispatch();
  const { isOpen } = useSelector(({ layout }) => layout.sidebar);

  return (
    <Drawer
      open={isOpen}
      PaperProps={{ sx: { flexDirection: "column", width: "100%" } }}
      onClose={() => dispatch(toggleSidebar())}
      anchor={"right"}
    >
      <Stack sx={{ p: 2 }} spacing={2}>
        <Stack direction="row" sx={{ justifyContent: "space-between" }}>
          <Image
            height={30}
            width={140}
            src={"/static/logo.png"}
            alt={"Logo"}
          />
          <IconButton
            sx={{ borderRadius: 4 }}
            onClick={() => dispatch(toggleSidebar())}
          >
            <Icon width={25} height={25} icon="ic:baseline-close" />
          </IconButton>
        </Stack>
        {Link?.map(({ name, to }, key) => (
          <LinkTypography key={key} name={name} to={to} />
        ))}
      </Stack>
    </Drawer>
  );
};

const LinkTypography = ({ name, to }) => {
  return (
    <Button variant="text" href={to} sx={{ textTransform: "capitalize" }}>
      <Typography variant="subtitle2">{name}</Typography>
    </Button>
  );
};

export default MobileNavbar;
