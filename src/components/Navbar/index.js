import React, { useEffect, useState } from "react";
// Materiel ui
import { AppBar } from "@mui/material";
// Hooks
import { useResponsive } from "../../hooks";
import MobileNavbar from "./mobile_navbar";
import DesktopNavbar from "./desktop_navbar";

// ----------------------------
const _link = [
  { name: "Home", to: "/" },
  { name: "About", to: "#about" },
  { name: "Skills", to: "#skills" },
  { name: "Projects", to: "#projects" },
  { name: "Contact", to: "#contact" },
];
// ----------------------------

export default function Navbar() {
  // Hooks
  const isMobile = useResponsive("down", "md");
  // Making top bar transparent
  const [transparent, setTransparent] = useState(true);

  useEffect(() => {
    const handleScroll = () => setTransparent(window.scrollY === 0);
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []);

  return (
    <AppBar
      sx={{
        background: (theme) =>
          !transparent ? theme.palette.background.default : "transparent",
        boxShadow: !transparent && "0 1px 2px -1px rgb(26 77 160 / 12%)",
        alignItems: "center",
      }}
      elevation={0}
      position="fixed"
    >
      {isMobile ? (
        <MobileNavbar Link={_link} />
      ) : (
        <DesktopNavbar Link={_link} />
      )}
    </AppBar>
  );
}
