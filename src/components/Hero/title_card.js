import React from "react";
// Mui
import { Card, Stack, Typography } from "@mui/material";
// Type Animation
import { TypeAnimation } from "react-type-animation";

const _titles = [
  "FULLSTACK DEVELOPER",
  1000,
  "MERN STACK DEVELOPER",
  1000,
  "FRONTEND DEVELOPER",
  1000,
  "MOBILE DEVELOPER",
  1000,
  "UI/UX DESIGNER",
];

const TitleCard = () => {
  return (
    <Stack
      spacing={1}
      sx={{
        mt: 4,
        display: "inline-flex",
        alignItems: "flex-end",
        borderRadius: 2,
        p: 2.5,
        boxShadow: "0 0 20px rgba(0,0,0,.1)",
      }}
    >
      <Typography sx={{ color: "#313bac", fontWeight: 600, fontSize: 18 }}>
        <TypeAnimation sequence={_titles} repeat={Infinity} speed={5} />
      </Typography>
    </Stack>
  );
};

export default TitleCard;
