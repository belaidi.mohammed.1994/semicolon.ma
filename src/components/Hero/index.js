import React, { useState } from "react";
import { motion } from "framer-motion";
// @mui
import { Stack, Container, styled, Fab, Grid, Box } from "@mui/material";
import { Icon } from "@iconify/react";
// components
import TitleCard from "./title_card";
import HelloCard from "./hello_card";

// ----------------------------------------------------------------------
const RootStyle = styled("div")(({ theme }) => ({
  background: "#EDF2F8",
  padding: theme.spacing(10, 0),
  [theme.breakpoints.up("md")]: {
    height: "100vh",
    padding: 0,
  },
}));
// ----------------------------------------------------------------------

export default function HeroSection() {
  return (
    <RootStyle>
      <Container
        sx={{
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Grid sx={{ mt: { md: 10 } }} container spacing={2}>
          <Grid item sm={12} md={3}>
            <HelloCard />
            <TitleCard />
          </Grid>
          <Grid item sm={12} md={6}>
            <Stack sx={{ justifyContent: "center", alignItems: "center" }}>
              <img
                style={{ width: 400, height: 400, borderRadius: "50%" }}
                src={"/static/my_image.png"}
                alt={"My image"}
              />
            </Stack>
          </Grid>
          <Grid item sm={12} md={3}>
            <Frameworks />
          </Grid>
        </Grid>
      </Container>
    </RootStyle>
  );
}

const Frameworks = () => {
  return (
    <motion.div
      initial={{ rotate: -10,  scale: 0 }}
      animate={{
        scale: 1,
        rotate: 0,
        transition: {
          type: "spring",
          stiffness: 260,
          damping: 20,
        },
      }}
    >
      <motion.div
        initial={{ x: 10 }}
        animate={{
          x: [-10, 10, -10],
          y: [-5, 5, -5],
          transition: {
            duration: 10,
            repeat: Infinity,
            repeatType: "reverse",
          },
        }}
      >
        <FrameworkSkill key={0} size={80} image={"/static/skills/nodejs.png"} />
        <FrameworkSkill
          key={2}
          style={{ p: 5 }}
          size={130}
          image={"/static/skills/react.png"}
        />
        <FrameworkSkill key={3} size={85} image={"/static/skills/mui.png"} />
      </motion.div>
    </motion.div>
  );
};

const FrameworkSkill = ({ size, image, style }) => {
  return (
    <Stack sx={style}>
      <Fab sx={{ width: size, height: size, background: "#fff" }}>
        <img style={{ width: size * 0.6 }} src={image} alt={"My image"} />
      </Fab>
    </Stack>
  );
};
