import * as React from "react";
// Mui
import { Fab, Stack } from "@mui/material";
import { Icon } from "@iconify/react";
// Components

// ----------------------------
const _social = [
  { name: "linkedin", icon: "ion:social-linkedin", to: "/" },
  { name: "github", icon: "ion:social-github", to: "/" },
  { name: "twitter", icon: "ion:social-twitter", to: "/" },
  { name: "facebook", icon: "ion:social-facebook", to: "/" },
  { name: "whatsapp", icon: "ion:social-whatsapp", to: "/" },
];
// ----------------------------

const SocialFab = () => {
  return (
    <Stack sx={{ position: "fixed", bottom: 60, left: 20 }} spacing={2}>
      {_social.map(({ icon }, idx) => (
        <Fab
          key={idx}
          sx={{
            width: 40,
            height: 40,
            background: "#fff",
            border: "1px solid #e4e4e4",
          }}
        >
          <Icon width={20} height={20} icon={icon} color="#6b7688" />
        </Fab>
      ))}
    </Stack>
  );
};

export default SocialFab;
