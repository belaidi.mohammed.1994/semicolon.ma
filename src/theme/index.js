import { Inter } from "next/font/google";
// Mui
import { createTheme } from "@mui/material/styles";
import palette from "./palette";
import breakpoints from "./breakpoints";
import shadows from "./shadows";
import shape from "./shape";
import typography from "./typography";

export const inter = Inter({
  weight: ["300", "400", "500", "700"],
  subsets: ["latin"],
  display: "swap",
  fallback: ["Helvetica", "Arial", "sans-serif"],
});

// Create a theme instance.
const theme = createTheme({
  palette,
  breakpoints,
  shadows,
  shape,
  typography,
});

export default theme;
