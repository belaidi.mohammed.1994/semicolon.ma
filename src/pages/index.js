import * as React from "react";
// Mui
import { Box } from "@mui/material";
// Components
import Navbar from "../components/Navbar";
import HeroSection from "../components/Hero";
import SocialFab from "../components/Social_fab";
import About from "../components/About";
import Skills from "../components/Skills";
import PortfolioSection from "../components/Portfolio";

const Index = () => {
  return (
    <Box>
      <Navbar />
      <HeroSection />
      <About />
      <Skills />
      <PortfolioSection />
      <SocialFab />
    </Box>
  );
};

export default Index;
